﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateFib calculate = new CalculateFib();
            Console.WriteLine("Write input number:");
            int inputNumber =Convert.ToInt32( Console.ReadLine());
            int[] res = calculate.getFibonacciArray(inputNumber,2000);
            string s = string.Empty;
            for (int i = 0; i < res.Length; i++)
            {
                s = s + res[i];
            }
            Console.Write(s.TrimStart('0'));
            Console.ReadLine();
        }

        public class CalculateFib
        {
            public int[] getFibonacciArray(int n, int size)
            {
                // Return F(n) in the form of an array, with (size + 1) elements
                int[] fibNr1 = new int[size];
                int[] fibNr2 = new int[size];
                int[] fibResultArr = new int[size + 1];

                //  set up
                for (int i = 0; i < size; i++)
                {
                    fibNr1[i] = fibNr2[i] = fibResultArr[i] = 0;
                }

                if (n == 0)
                {
                    // return fibNr1;
                    return (customAdd(fibNr1, fibNr1));
                }

                if (n == 1)
                {
                    fibNr2[size - 1] = 1;
                    // return fibNr2;
                    return (customAdd(fibNr1, fibNr2));
                }

                // linear algorithm
                fibNr2[size - 1] = 1;
                for (int i = 0; i < n - 1; i++)
                {
                    fibResultArr = customAdd(fibNr1, fibNr2);
                    fibNr1 = fibNr2;

                    int[] fibArr2Temp = new int[fibNr2.Length];
                    for (int j = 0; j < fibNr2.Length; j++)
                    {
                        fibArr2Temp[j] = fibResultArr[j + 1];
                    }
                    fibNr2 = fibArr2Temp;
                }

                return fibResultArr;
            }

            private static int[] customAdd(int[] arr1, int[] arr2)
            {
                int size = arr1.Length;
                int[] arrTotal = new int[size + 1];
                for (int i = 0; i < size; i++)
                {
                    arrTotal[i] = 0;
                }

                int remaider = 0;
                for (int i = size - 1; i >= 0; i--)
                {
                    int temp = arr1[i] + arr2[i] + remaider;
                    arrTotal[i + 1] = temp % 10;
                    remaider = temp / 10;
                }
                arrTotal[0] = remaider;

                return arrTotal;
            }
        }
    }
}
